<?php

declare(strict_types=1);

namespace CIConfigGen\Exception;

use Exception;

final class ShouldNotHappenException extends Exception
{
}
